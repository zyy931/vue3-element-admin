import request from "@/utils/request";
import { AxiosPromise } from "axios";

import { UserInfo } from "./types";

export function getUserInfo(): AxiosPromise<UserInfo> {
  return request({
    url: '/youlai-admin/api/v1/users/me',
    method: 'get'
  })
}