import { createApp } from 'vue'
import App from './App.vue'
import ElementPlus from 'element-plus'
import 'element-plus/theme-chalk/index.css'
//注册svg
import 'virtual:svg-icons-register' // 引入svg icon注册脚本
// import SvgIcon from './assets/icons/SvgIcon.vue'
//Pinia全局注册
import { setupStore } from '@/store';
import router from "@/router";
// 国际化
import i18n from "@/lang/index";

const app = createApp(App)


//注册全局组件
// app.component('svg-icon', SvgIcon)
app.use(ElementPlus)
// 全局挂载
setupStore(app);
app.use(router)
app.use(i18n)
app.mount('#app')
