import { loadEnv, UserConfig, ConfigEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path'
import { createSvgIconsPlugin } from 'vite-plugin-svg-icons'
//vue3等插件 hooks 自动引入
import AutoImport from 'unplugin-auto-import/vite'
export default ({ command, mode }: ConfigEnv): UserConfig => {
  // 获取 .env 环境配置文件
  const env = loadEnv(mode, process.cwd())
  return (
    {
      plugins: [
        vue(),
        createSvgIconsPlugin({
          // 指定需要缓存的图标文件夹
          iconDirs: [path.resolve(process.cwd(), '/src/assets/icons')],
          // 指定symbolId格式
          symbolId: 'icon-[dir]-[name]'
        }),
        AutoImport({
          imports: ['vue', 'vue-router', 'vuex', '@vueuse/head'],
          // 可以选择auto-import.d.ts生成的位置，使用ts建议设置为'src/auto-import.d.ts'
          dts: 'src/auto-import.d.ts'
        }),
      ],
      server: {
        host: 'localhost',
        port: Number(env.VITE_APP_PORT),
        open: true,  // 启动是否自动打开浏览器
        proxy: {
          [env.VITE_APP_BASE_API]: {
            target: 'https://api.youlai.tech',
            changeOrigin: true,
            rewrite: path => path.replace(new RegExp('^' + env.VITE_APP_BASE_API), '')
          }
        },
      },
      resolve: {
        alias: {
          "@": path.resolve(__dirname, "./src")
        }
      }
    }
  )

}
